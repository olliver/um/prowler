/*
 * (c) 2016 Ultimaker B.V.
 * Author: O.M. Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-license AGPL-3+
 *
 * This stub implements a very limited subset of the tinyg parser and reporting.
 *
 */
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

void print_usage(const char *self)
{
	printf("%s usage:\n", self);
	puts("\t--help, h for help");
	puts("\t--input_file, i <filename>; if omitted uses stdin");
	puts("\t--output_file, o <filename>; if omitted uses stdout");

	exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	FILE *input = stdin;
	FILE *output = stdout;
	ssize_t read;
	char *line;
	size_t len;

	if (argc < 1)
		print_usage(argv[0]);

	while (1) {
		int c;
		static struct option long_options[] = {
			{"input_file", required_argument, 0, 'i'},
			{"output_file", required_argument, 0, 'o'},
			{"help", no_argument, 0, 'h'},
			{0, 0, 0, 0} /* sentinel */
		};
		int option_index = 0;

		c = getopt_long(argc, argv, "i:o:h", long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
		case 'i':
			input = fopen(optarg, "r");
			if (!input) {
				perror("Open input");
				exit(EXIT_FAILURE);
			}
			break;
		case 'o':
			output = fopen(optarg, "w");
			if (!output) {
				perror("Open output");
				exit(EXIT_FAILURE);
			}
			break;
		case '?':
			break;
		case 'h': /* fall through */
		default:
			print_usage(argv[0]);
			break;
		}
	}

	

	fclose(input);
	fclose(output);

	return EXIT_SUCCESS;
}
